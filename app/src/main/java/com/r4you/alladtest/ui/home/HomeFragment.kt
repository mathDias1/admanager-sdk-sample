package com.r4you.alladtest.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerAdView
import com.r4you.alladtest.R

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    lateinit var adView: AdManagerAdView
    lateinit var mAdManagerAdView: AdManagerAdView

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val textView: TextView = root.findViewById(R.id.text_home)
        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })


        val adLayout = root.findViewById(R.id.adFrameTwo) as LinearLayout
        val ctx = getActivity()
        adView = AdManagerAdView(ctx)
        adView.setAdSizes(AdSize(300, 250))
        adView.adUnitId = "/6499/example/banner"
        adLayout.addView(adView);
        val adRequest = AdManagerAdRequest.Builder().build() as AdManagerAdRequest
        adView.loadAd(adRequest)
        return root
    }
}