package com.r4you.alladtest.ui.dashboard

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerAdView
import com.r4you.alladtest.R
import org.prebid.mobile.*
import org.prebid.mobile.addendum.AdViewUtils
import org.prebid.mobile.addendum.PbFindSizeError


class DashboardFragment : Fragment() {
    internal var adUnit: AdUnit? = null
    private lateinit var dashboardViewModel: DashboardViewModel
    internal var mainContext: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mainContext = context
    }
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val textView: TextView = root.findViewById(R.id.text_dashboard)
        dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        val adFrame = root.findViewById(R.id.adFrame) as FrameLayout
        adFrame.removeAllViews()
        val dfpAdView = AdManagerAdView(this.mainContext)

        PrebidMobile.setPrebidServerHost(Host.APPNEXUS)
        PrebidMobile.setPrebidServerAccountId("bfa84af2-bd16-4d35-96ad-31c6bb888df0")
        dfpAdView.setAdUnitId("/19968336/PrebidMobileValidator_Banner_All_Sizes")

        adUnit = BannerAdUnit("6ace8c7d-88c0-4623-8117-75bc3f0a2e45", 300, 250)
        val adSizeTest = AdSize(300, 250)
        dfpAdView.setAdSizes(adSizeTest)
        adFrame.addView(dfpAdView)
        val builder = AdManagerAdRequest.Builder()
        dfpAdView.setAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                Log.i("TAG","ON LOADED")
                AdViewUtils.findPrebidCreativeSize(dfpAdView, object : AdViewUtils.PbFindSizeListener {
                    override fun success(width: Int, height: Int) {
                        Log.i("TAG","ON LOAD 2 $width")
                        Log.i("TAG","ON LOAD 3 $height")
                        dfpAdView.setAdSizes(AdSize(width, height))
                    }
                    override fun failure(error: PbFindSizeError) {
                        dfpAdView.setAdSizes(AdSize(300, 250))
                        Log.i("TAG", "error: $error")
                    }
                })
            }
        })


        adUnit!!.fetchDemand(builder, object : OnCompleteListener {
            override fun onComplete(resultCode: ResultCode) {
                Log.i("TAG","ON COMPLETED $resultCode")
                dfpAdView.loadAd(builder.build())
            }
        })





/*
        mAdManagerAdView = root.findViewById(R.id.publisherAdView)
        PrebidMobile.setPrebidServerHost(Host.APPNEXUS)
        PrebidMobile.setPrebidServerAccountId("bfa84af2-bd16-4d35-96ad-31c6bb888df0")

        adUnit = BannerAdUnit("6ace8c7d-88c0-4623-8117-75bc3f0a2e45", 300, 250)
        Log.i("TAG","IMPLEMENTS")
        mAdManagerAdView.setAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                Log.i("TAG","ON LOADED")
                AdViewUtils.findPrebidCreativeSize(mAdManagerAdView, object : AdViewUtils.PbFindSizeListener {
                    override fun success(width: Int, height: Int) {
                        mAdManagerAdView.setAdSizes(AdSize(width, height))
                    }
                    override fun failure(error: PbFindSizeError) {
                        Log.i("TAG", "error: $error")
                    }
                })
            }
        })
        mAdManagerAdView.setAdSizes(AdSize(300, 250))
        val builder = AdManagerAdRequest.Builder()


        adUnit!!.fetchDemand(builder, object : OnCompleteListener {
            override fun onComplete(resultCode: ResultCode) {
                Log.i("TAG","ON COMPLETED")
                mAdManagerAdView.loadAd(builder.build())
            }
        })*/
        return root
    }

    /*internal fun createDFPBanner(size: String) {
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)


        val dfpAdView = AdManagerAdView()
        val wAndH = size.split("x".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val width = 300
        val height = 250
        if (width == 300 && height == 250) {
            dfpAdView.setAdUnitId("/19968336/PrebidMobileValidator_Banner_All_Sizes")
            adUnit = BannerAdUnit("6ace8c7d-88c0-4623-8117-75bc3f0a2e45", width, height)
        }

        dfpAdView.setAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()

                AdViewUtils.findPrebidCreativeSize(dfpAdView, object : AdViewUtils.PbFindSizeListener {
                    override fun success(width: Int, height: Int) {
                        dfpAdView.setAdSizes(AdSize(width, height))

                    }

                    override fun failure(error: PbFindSizeError) {
                        Log.d("MyTag", "error: $error")
                    }
                })

            }
        })

        dfpAdView.setAdSizes(AdSize(width, height))
        adFrame.addView(dfpAdView)
        val builder = AdManagerAdRequest.Builder()

        adUnit!!.setAutoRefreshPeriodMillis(100000)
        adUnit!!.fetchDemand(builder, object : OnCompleteListener {
            override fun onComplete(resultCode: ResultCode) {
                dfpAdView.loadAd(builder.build())
            }
        })
        //endregion
    }*/
}